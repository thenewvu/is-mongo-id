'use strict';

/**
 * Check if a string is a mongo id.
 * @param id
 * @returns {boolean}
 */
function isMongoId(v, o, done) {
  done((typeof v == 'string' && v.length == 24) === o);
}

module.exports = isMongoId;
